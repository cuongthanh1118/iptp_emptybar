package demo.entities;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import demo.util.DateAdapter;

@XmlAccessorType(XmlAccessType.FIELD)
public class Bar {
	@XmlAttribute(name = "id")
	private int id;

	@XmlAttribute(name = "startdate")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date startDate;

	@XmlAttribute(name = "enddate")
	@XmlJavaTypeAdapter(DateAdapter.class)
	private Date endDate;

	@XmlAttribute(name = "name")
	private String name;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


}
