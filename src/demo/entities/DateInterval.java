package demo.entities;

public class DateInterval {
	private long fromMiliSecond;
	private long toMiliSecond;

	public long getFromMiliSecond() {
		return fromMiliSecond;
	}

	public void setFromMiliSecond(long fromMiliSecond) {
		this.fromMiliSecond = fromMiliSecond;
	}

	public long getToMiliSecond() {
		return toMiliSecond;
	}

	public void setToMiliSecond(long toMiliSecond) {
		this.toMiliSecond = toMiliSecond;
	}

}
