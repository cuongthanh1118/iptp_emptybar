package demo.util;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import demo.entities.DateInterval;

/**
 * @author cuongnguyen
 *
 */
public class Util {
	
	public static String dateToString(Date d) {
		try {
			SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
			 return formatter.format(d);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}  
	}
	
	public static String dateIntervalToString(DateInterval di) {
		try {
			return String.format(" [ %s --> %s ]",
					Util.dateToString(new Date(di.getFromMiliSecond())),
					Util.dateToString(new Date(di.getToMiliSecond())));
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}  
	}
	
	public static long dateToMiliSecond(Date d) {
		try {
			   Timestamp ts=new Timestamp(d.getTime());  
               return ts.getTime();                    
		} catch (Exception e) {
			e.printStackTrace();
			return 0;
		}  
	}
}
