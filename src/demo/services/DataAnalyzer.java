package demo.services;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import demo.entities.Bar;
import demo.entities.DateInterval;
import demo.entities.Response;
import demo.util.Util;

/**
 * @author cuongnguyen
 *
 */
public class DataAnalyzer {
	
	private Response responseData;

	/**
	 * Set input .xml file data 
	 * it will parse xml to data objects
	 * @param filePath - .xml file path
	 */
	public void setInputFile(String filePath) {
		try {
			responseData = parseResponeDataFromXmlFile(filePath);
		} catch (Exception e) {
			e.printStackTrace();
			responseData = null;
		}
	
	}
	
	private static Response parseResponeDataFromXmlFile(String filePath) throws JAXBException {
		File file = new File(filePath);
		JAXBContext jaxbContext = JAXBContext.newInstance(Response.class);

		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		Response reponseData = (Response) jaxbUnmarshaller.unmarshal(file);
		return reponseData;
	}
	
	public List<DateInterval> getEmptyIntervals2() {
		try {
			if (responseData == null || responseData.getBars() == null || 
					responseData.getBars().size() == 0) {
				throw new Exception("Don't have Response Data");
			}
			
			List<Bar> bars = responseData.getBars();
			
			Comparator<Bar> compareByStartDate = (Bar o1, Bar o2) -> o1.getStartDate().compareTo( o2.getStartDate() );
			 
			Collections.sort(bars, compareByStartDate);
			
			
			List<DateInterval> emptyIntervals = new ArrayList<DateInterval>();
			Bar b1,b2;
			b1 = bars.get(0);
			for (int i= 1; i< bars.size();i++) {
				b2 = bars.get(i);
				
				if(b1.getEndDate().compareTo(b2.getStartDate())< 0) {
					DateInterval interval = new DateInterval();
					interval.setFromMiliSecond(Util.dateToMiliSecond(b1.getEndDate()));
					interval.setToMiliSecond(Util.dateToMiliSecond(b2.getStartDate()));
					emptyIntervals.add(interval);
				}
				
				if(b1.getEndDate().compareTo(b2.getEndDate())> 0) {
					continue;
				}
				b1 = b2;
				
			}
			
			return emptyIntervals;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<DateInterval>();
		}

	}

	
	/**
	 * find all the empty intervals (during which no people are working).
	 * @return List of empty intervals
	 */
	public List<DateInterval> getEmptyIntervals() {
		try {
			if (responseData == null || responseData.getBars() == null || 
					responseData.getBars().size() == 0) {
				throw new Exception("Don't have Response Data");
			}
			
			List<Bar> bars = responseData.getBars();
			// get min, max date
			Bar maxEndDateBar = Collections.max(bars, Comparator.comparing(b -> b.getEndDate()));
			Bar minStartDateBar = Collections.min(bars, Comparator.comparing(b -> b.getStartDate()));

			long minMiliSecond = Util.dateToMiliSecond(minStartDateBar.getStartDate());
			long maxMiliSecond = Util.dateToMiliSecond(maxEndDateBar.getStartDate());

			DateInterval maxInterval = new DateInterval();
			maxInterval.setFromMiliSecond(minMiliSecond);
			maxInterval.setToMiliSecond(maxMiliSecond);
	
			// Calculate empty intervals in range(minSecond,maxSecond)
			List<DateInterval> emptyIntervals = new ArrayList<DateInterval>();
			emptyIntervals.add(maxInterval);

			for (Bar b : bars) {
				emptyIntervals = calculateEmptyIntervals(b, emptyIntervals);
			}
			
			return emptyIntervals;
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<DateInterval>();
		}

	}

	/**
	 * find all the empty intervals with range from min to max dateTime .
	 * @param bars - all people working intervals
	 * @param emptyIntervals - list contain min, max datetime
	 * @return List of empty intervals
	 */
	private List<DateInterval> calculateEmptyIntervals(Bar bar, List<DateInterval> emptyIntervals) throws Exception{
		List<DateInterval> newEmptyIntervals = new ArrayList<DateInterval>();
		List<Integer> removeItems = new ArrayList<Integer>();
		try {
			long barFrSec = Util.dateToMiliSecond(bar.getStartDate());
			long barToSec = Util.dateToMiliSecond(bar.getEndDate());
			
			for (int i = 0; i < emptyIntervals.size(); i++) {
				DateInterval di = emptyIntervals.get(i);
				// bar range bigger or equal
				if (barFrSec <= di.getFromMiliSecond() && di.getToMiliSecond() <= barToSec) {
					removeItems.add(i);
					
					if (barFrSec == di.getFromMiliSecond() && di.getToMiliSecond() == barToSec) {
						break;
					}
					// case bar range bigger, need to consider with next interval
					continue;
				}

				// bar range in middle
				if (di.getFromMiliSecond() <= barFrSec && barToSec <= di.getToMiliSecond()) {
					processBetween(newEmptyIntervals, barFrSec, barToSec, di);
					removeItems.add(i);
					break;
				}

				// bar range on right
				if (di.getFromMiliSecond() < barFrSec && barFrSec <= di.getToMiliSecond() && 
						di.getToMiliSecond() < barToSec) {
					DateInterval di1 = new DateInterval();
					di1.setFromMiliSecond(di.getFromMiliSecond());
					di1.setToMiliSecond(barFrSec);
					
					newEmptyIntervals.add(di1);
					removeItems.add(i);
					continue;
				}

				// bar range on left
				if (barFrSec < di.getFromMiliSecond() && 
						di.getFromMiliSecond() < barToSec && barToSec < di.getToMiliSecond()) {
					DateInterval di1 = new DateInterval();
					di1.setFromMiliSecond(barToSec);
					di1.setToMiliSecond(di.getToMiliSecond());
					
					newEmptyIntervals.add(di1);
					removeItems.add(i);
					continue;
				}
			}

			// remove all same interval
			for (int i = 0; i < emptyIntervals.size(); i++) {
				if (!removeItems.contains(i)) {
					newEmptyIntervals.add(emptyIntervals.get(i));
				}
			}
			
			// sort results
			newEmptyIntervals.sort(Comparator.comparing(DateInterval::getFromMiliSecond));
			
		} catch (Exception e) {
			e.printStackTrace();
			
			newEmptyIntervals = new ArrayList<DateInterval>();
		}

		return newEmptyIntervals;
	}

	// case bar range in between empty interval
	private void processBetween(List<DateInterval> newEmptyIntervals, long barFrsec, long barToSec,
			DateInterval selectedDateInterval) throws Exception {
		DateInterval di1 = new DateInterval();
		DateInterval di2 = null;
		if (selectedDateInterval.getFromMiliSecond() == barFrsec) {
			di1.setFromMiliSecond(barToSec);
			di1.setToMiliSecond(selectedDateInterval.getToMiliSecond());

		} else if (barToSec == selectedDateInterval.getToMiliSecond()) {
			di1.setFromMiliSecond(selectedDateInterval.getFromMiliSecond());
			di1.setToMiliSecond(barFrsec);

		} else {
			di1.setFromMiliSecond(selectedDateInterval.getFromMiliSecond());
			di1.setToMiliSecond(barFrsec);

			di2 = new DateInterval();
			di2.setFromMiliSecond(barToSec);
			di2.setToMiliSecond(selectedDateInterval.getToMiliSecond());
		}

		newEmptyIntervals.add(di1);
		if (di2 != null) {
			newEmptyIntervals.add(di2);
		}
	}

}
