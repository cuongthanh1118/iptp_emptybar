package unittest.demo.services;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.Test;

import demo.entities.DateInterval;
import demo.services.DataAnalyzer;
import demo.util.Util;

class DataAnalyzerTest {
	DataAnalyzer analyzer = new DataAnalyzer();
	
	@Test
	void testWrongFile() {
		analyzer.setInputFile("");
		List<DateInterval> emptyIntervals = analyzer.getEmptyIntervals2();
		assertEquals(0, emptyIntervals.size());
	}
	
	@Test
	void testFileWithNoBar() {
		analyzer.setInputFile("xml_data/test/data_java_test1.xml");
		List<DateInterval> emptyIntervals = analyzer.getEmptyIntervals2();
		assertEquals(0, emptyIntervals.size());
	}
	
	@Test
	void testFileWithBar0() {
		analyzer.setInputFile("xml_data/test/data_java_test2.xml");
		List<DateInterval> emptyIntervals = analyzer.getEmptyIntervals2();
		assertEquals(0, emptyIntervals.size());
	}
	
	@Test
	void testFileWithBar1() {
		analyzer.setInputFile("xml_data/test/data_java_test3.xml");
		List<DateInterval> emptyIntervals = analyzer.getEmptyIntervals2();

		assertEquals(1, emptyIntervals.size());
		assertNotNull(emptyIntervals.get(0).getFromMiliSecond());
		assertNotNull(emptyIntervals.get(0).getToMiliSecond());
		
	
		assertTrue("2015-01-27 14:00:00".equals(
				Util.dateToString(new Date(emptyIntervals.get(0).getFromMiliSecond()))));
		assertTrue("2015-01-27 16:00:00".equals(
				Util.dateToString(new Date(emptyIntervals.get(0).getToMiliSecond()))));
	
	}
	
	@Test
	void testFileWithBar2() {
		analyzer.setInputFile("xml_data/test/data_java_test4.xml");
		List<DateInterval> emptyIntervals = analyzer.getEmptyIntervals2();
		

		assertEquals(2, emptyIntervals.size());
		assertNotNull(emptyIntervals.get(0).getFromMiliSecond());
		assertNotNull(emptyIntervals.get(0).getToMiliSecond());
		
	
		assertTrue("2015-01-27 08:00:00".equals(
				Util.dateToString(new Date(emptyIntervals.get(0).getFromMiliSecond()))));
		assertTrue("2015-01-27 09:00:00".equals(
				Util.dateToString(new Date(emptyIntervals.get(0).getToMiliSecond()))));
		
		assertNotNull(emptyIntervals.get(1).getFromMiliSecond());
		assertNotNull(emptyIntervals.get(1).getToMiliSecond());
		
		assertTrue("2015-01-27 14:00:00".equals(
				Util.dateToString(new Date(emptyIntervals.get(1).getFromMiliSecond()))));
		assertTrue("2015-01-27 16:00:00".equals(
				Util.dateToString(new Date(emptyIntervals.get(1).getToMiliSecond()))));
	
	}
	
	@Test
	void testFileWithBar3() {
		analyzer.setInputFile("xml_data/test/data_java_test5.xml");
		List<DateInterval> emptyIntervals = analyzer.getEmptyIntervals2();
		
		assertEquals(2, emptyIntervals.size());
		assertNotNull(emptyIntervals.get(0).getFromMiliSecond());
		assertNotNull(emptyIntervals.get(0).getToMiliSecond());
//		
		assertTrue("2015-01-27 08:30:00".equals(
				Util.dateToString(new Date(emptyIntervals.get(0).getFromMiliSecond()))));
		assertTrue("2015-01-27 09:00:00".equals(
				Util.dateToString(new Date(emptyIntervals.get(0).getToMiliSecond()))));
			
		assertNotNull(emptyIntervals.get(1).getFromMiliSecond());
		assertNotNull(emptyIntervals.get(1).getToMiliSecond());
		
		assertTrue("2015-01-27 14:00:00".equals(
				Util.dateToString(new Date(emptyIntervals.get(1).getFromMiliSecond()))));
		assertTrue("2015-01-27 16:00:00".equals(
				Util.dateToString(new Date(emptyIntervals.get(1).getToMiliSecond()))));
	
	}

}
