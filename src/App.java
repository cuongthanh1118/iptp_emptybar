import java.util.Date;
import java.util.List;

import demo.entities.DateInterval;
import demo.services.DataAnalyzer;
import demo.util.Util;

/**
 * @author cuongnguyen
 *
 */
public class App {
	
	public static void main(String[] args) {
		try {
			DataAnalyzer analyzer =  new DataAnalyzer();
			
			analyzer.setInputFile("xml_data/data_java.xml");
			long t =  System.currentTimeMillis();
		
			List<DateInterval> emptyIntervals =	analyzer.getEmptyIntervals();
					
			System.out.println(" All empty intervals : " + (System.currentTimeMillis() -t) );
						
			if(emptyIntervals.size() == 0) {
				System.out.println(" None ");
			}
			// print results
			int i=1;
			for (DateInterval dateInterval : emptyIntervals) {
				System.out.println(String.format(" No.%s : %s  ", i++ ,
						Util.dateIntervalToString(dateInterval)));
			}
			
	t =  System.currentTimeMillis();
			
			List<DateInterval> emptyIntervals2 =	analyzer.getEmptyIntervals();
					
			System.out.println(" All empty intervals2 : " + (System.currentTimeMillis() -t) );
			
			if(emptyIntervals2.size() == 0) {
				System.out.println(" None ");
			}
			// print results
			 i=1;
			for (DateInterval dateInterval : emptyIntervals2) {
				System.out.println(String.format(" No.%s : %s  ", i++ ,
						Util.dateIntervalToString(dateInterval)));
			}
			
		  } catch (Exception e) {
			e.printStackTrace();
		  }
	}
}
